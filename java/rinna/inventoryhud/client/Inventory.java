package rinna.inventoryhud.client;

// import net.minecraft.client.Minecraft;
// import net.minecraft.client.gui.ScaledResolution;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Inventory extends RenderingSteps {
	
//	private static Minecraft mc = Minecraft.getMinecraft();
//	private static ScaledResolution sr = new ScaledResolution(mc);

	public static int LocX;
	public static int LocY;

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void renderInventory(RenderGameOverlayEvent event) {
		
		/*
		LocX = ConfigInventoryHud.hudPositionX / sr.getScaleFactor();
		LocY = ConfigInventoryHud.hudPositionY / sr.getScaleFactor();
		*/
		
			// NonNullList<ItemStack> items = mc.player.inventory.mainInventory;

			boxrender(LocX, LocY);
			itemrender(LocX, LocY);
	}

	

}
