package rinna.inventoryhud.client;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;

public class RenderingSteps {
	
	public static Minecraft mc = Minecraft.getMinecraft();
	
	private final static ResourceLocation resource = new ResourceLocation("textures/gui/container/shulker_box.png");

	public void boxrender(int x, int y) {
		preboxrender();
		
		mc.renderEngine.bindTexture(resource);
		mc.ingameGUI.drawTexturedModalRect(x, y, 7, 17, 162, 54);
		
		postboxrender();
	}
	
	public static void preboxrender() {
		GL11.glPushMatrix();
        GlStateManager.pushMatrix();
        GlStateManager.disableAlpha();
        GlStateManager.clear(256);
        GlStateManager.enableBlend();
	}
	
	public static void postboxrender() {
    	GlStateManager.disableBlend();
        GlStateManager.disableDepth();
        GlStateManager.disableLighting();
        GlStateManager.enableDepth();
        GlStateManager.enableAlpha();
        GlStateManager.popMatrix();
        GL11.glPopMatrix();
    }
	
	public void itemrender(int x, int y) {
		
		NonNullList<ItemStack> items = mc.player.inventory.mainInventory;
		
		for (int size = items.size(), item = 9; item < size; ++item) {
			int slotx = x + 1 + item % 9 * 18;
			int sloty = y + 1 + (item / 9 - 1) * 18;
			
			preitemrender();
			
			mc.getRenderItem().renderItemAndEffectIntoGUI(items.get(item), slotx, sloty);
            mc.getRenderItem().renderItemOverlays(mc.fontRenderer, items.get(item), slotx, sloty);
            
            postitemrender();
		}
	}
	public static void preitemrender() {
		GL11.glPushMatrix();
		GL11.glDepthMask(true);
		GlStateManager.clear(256);
		
        GlStateManager.disableDepth();
        GlStateManager.enableDepth();
		
		RenderHelper.enableStandardItemLighting();
    	GlStateManager.scale(1, 1, 0.01f);
	}
	
	public static void postitemrender() {
		GlStateManager.scale(1, 1, 1f);
        RenderHelper.disableStandardItemLighting();
		GlStateManager.enableAlpha();
		GlStateManager.disableBlend();
		GlStateManager.disableLighting();
		GlStateManager.scale(0.5D, 0.5D, 0.5D);
		GlStateManager.disableDepth();

		GlStateManager.enableDepth();
		GlStateManager.scale(2.0F, 2.0F, 2.0F);
		GL11.glPopMatrix();
	}

}
