package rinna.inventoryhud;

import net.minecraftforge.fml.common.Mod;


@Mod(
	modid = InventoryHud.MODID,
	name = InventoryHud.NAME,
	version = InventoryHud.VERSION,
	acceptedMinecraftVersions = InventoryHud.ACCEPTEDVERSIONS,
	clientSideOnly = InventoryHud.CLIENTSIDED
)
public class InventoryHud {
	
	public static final String MODID = "inventoryhud";
	public static final String NAME = "Inventory HUD";
	
	public static final String VERSION = "1.0";
	public static final String ACCEPTEDVERSIONS = "{1.12.2}";
	
	public static final boolean CLIENTSIDED = true;
	
	
	/*
	@EventHandler
    public void onInit(FMLInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new TickHandler());
		MinecraftForge.EVENT_BUS.register(new KeyRegister());
	}
	*/
	
	/*
	@EventHandler
	public void PostInit(FMLPostInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(new Inventory());
	}
	*/

}
